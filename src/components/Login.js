import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./login.css";
import Button from 'react-bootstrap/Button';
import { auth } from "../firebase";



function Login() {
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');

const iniciarS = e=>{
  e.preventDefault();
  // aqui se conecta con firebase
  
  }
  
  const registrar = e=>{
      e.preventDefault();
  // aqui se conecta con firebase
  
  auth 
     .createUserWithEmailAndPassword(email, password)
     .then((auth)=>{
       // su correo ha sido creado satisfactoriamente
        console.log(auth)
        alert('Su cuenta ha sido creada satisfactoriamente')
     })
     .catch(error=>alert(error.message))
  
  }

  return (
    <div className="login">
      <div className="login_container">
    
        <form>
         <h1>LOGIN</h1>
          <h5>Correo Electronico</h5>
          <input type="text" placeholder="Digite  correo"  value={email} onChange={e => setEmail(e.target.value)}/><br/><br/>
          <h5>Contraseña</h5>
          <input type="password" placeholder="Digite contraseña" value={password} onChange={e=>setPassword(e.target.value)}/><br/><br/>
          <Button variant="primary" type="submit" onClick={iniciarS} block>Iniciar</Button>
          <p>Todos sus datos suministrados seran confidenciales.</p>   
          <Button variant="success" onClick={registrar} block>Crea una cuenta</Button>
          
        </form>
      </div>
    </div>
  );
}

export default Login;
